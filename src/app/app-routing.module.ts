import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { AddEditBookComponent } from './books/components/add-edit-book/add-edit-book.component';
import { BookComponent } from './books/components/book/book.component';
import { LoginComponent } from './books/components/login/login.component';
import { RegisterComponent } from './books/components/register/register.component';

const routes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'book/:bookId', component: BookComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'add-book', component: AddEditBookComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppRoutingModule { }
