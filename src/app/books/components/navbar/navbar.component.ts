import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  constructor(private http: HttpClient, private router: Router) { }
  decoded : any;
  role : any;
  isLoggedIn(): boolean{
    var token = localStorage.getItem("token");
    if(token!= undefined){
      this.decoded = jwt_decode(token);
      if(this.decoded.role != undefined){
        return true;
      }
    }
    return false;
  }

  isAdmin() : boolean{
    var token = localStorage.getItem("token");
    if(token != undefined){
      this.decoded = jwt_decode(token);
      if(this.decoded.role.includes("ROLE_ADMIN")){
        return true;
      }
    }
    return false;
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('username');
  }
 

}
