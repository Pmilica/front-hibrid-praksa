import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";


export function creationDateValidator(date: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      var currentDate = new Date();
      var dateCorrect = false;
      if(date < currentDate.getTime()){
        dateCorrect = true;
      }

      return !dateCorrect ? {forbiddenDate: {value: control.value}} : null;
    };
  }
  