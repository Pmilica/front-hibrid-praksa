import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAddAuthorComponent } from './form-add-author.component';

describe('FormAddAuthorComponent', () => {
  let component: FormAddAuthorComponent;
  let fixture: ComponentFixture<FormAddAuthorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormAddAuthorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAddAuthorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
