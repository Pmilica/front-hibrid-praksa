import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthorService } from '../../services/author.service';

@Component({
  selector: 'app-form-add-author',
  templateUrl: './form-add-author.component.html',
  styleUrls: ['./form-add-author.component.scss']
})
export class FormAddAuthorComponent implements OnInit {

  allAuthors: any = [];
  @Input() isModalVisible!: boolean;
  @Output() isVisible = new EventEmitter<boolean>();
  @Output() authorAdded = new EventEmitter<any>();

  validateAuthor = new FormGroup({
    firstName: new FormControl('', Validators.compose([
      Validators.required])),
    middleName: new FormControl(),
    lastName: new FormControl('', Validators.compose([
      Validators.required]))
  });
  constructor(private authorsService: AuthorService, private fb: FormBuilder,) { }

  ngOnInit(): void {
    this.authorsService.getAllAuthors().subscribe( authors => this.allAuthors = authors );  
    
  }
  
  handleCancel(): void {
    this.isModalVisible = false;
    this.isVisible.emit(this.isModalVisible);
  }

  handleOk(form: any){
    for (const key in this.validateAuthor.controls) {
      if (this.validateAuthor.controls.hasOwnProperty(key)) {
        this.validateAuthor.controls[key].markAsDirty();
        this.validateAuthor.controls[key].updateValueAndValidity();
      }
    }
   
    var author = {
      firstName: form.value.firstName,
      middleName: form.value.middleName,
      lastName: form.value.lastName
    }

    this.authorsService.addAuthor(author).subscribe( author => {
      this.authorAdded.emit(author);
    })
  }

 
}
