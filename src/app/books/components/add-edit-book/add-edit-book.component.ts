import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormControl,FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { en_US, NzI18nService } from 'ng-zorro-antd/i18n';
import { AuthorService } from '../../services/author.service';
import { BooksService } from '../../services/books.service';

@Component({
  selector: 'app-add-edit-book',
  templateUrl: './add-edit-book.component.html',
  styleUrls: ['./add-edit-book.component.scss']
})
export class AddEditBookComponent implements OnInit {

  listOfSelectedValue : any = [];
  books: any = [];
  allAuthors: any = [];
  date : any;
  dateFormat = 'yyyy-MM-dd';
  imageUrl : any;
  validateForm = new FormGroup({
    title: new FormControl('', Validators.compose([
      Validators.required])),
    description: new FormControl(''),
    authors: new FormControl(),
    imageUrl : new FormControl('', Validators.compose([
      Validators.required])),
    isbn: new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern('^(?=(?:\\D*\\d){10}(?:(?:\\D*\\d){3})?$)[\\d-]+$')])),
    quantity: new FormControl(null)
  });


  constructor(private booksService : BooksService, private authorsService: AuthorService, private fb: FormBuilder, private i18n: NzI18nService, private router: Router) {
    
  }
 
  submitForm(formValue : any): void {
    for (const key in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(key)) {
        this.validateForm.controls[key].markAsDirty();
        this.validateForm.controls[key].updateValueAndValidity();
      }
    }
    var book : any = {
      title : formValue.value.title,
      description : formValue.value.description,
      creationDate : this.formatDate(Date.parse(this.date)),
      isbn : formValue.value.isbn,
      authors : this.listOfSelectedValue,
      quantity : formValue.value.quantity,
      imageUrl: formValue.value.imageUrl
    }
    this.booksService.addBook(book).subscribe(book=>
      {
        this.router.navigate([`book/${book.id}`])
      });
  }
  ngOnInit(): void {
    this.booksService.getBooks().subscribe(books=>this.books=books);
    this.i18n.setLocale(en_US);
    this.authorsService.getAllAuthors().subscribe( authors => this.allAuthors = authors );  
    
  }
  getFullAuthorName(author : any){
    var fullName = author.firstName + ' ' + author.lastName;
    return fullName;
  }
  
  isModalVisible = false;
  openAuthorModal(){
    this.isModalVisible = true;
  }

  addAuthor(author: any){
    this.allAuthors.push(author);
  }

  setIsModalVisible(isVisible: boolean){
    this.isModalVisible = isVisible;
  }

  formatDate(date:any) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

}
