import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  userName : any;
  password : any;
  firstName : any;
  lastName : any;
  validateRegister = new FormGroup({
    userName: new FormControl('', Validators.compose([
      Validators.required])),
    password: new FormControl('', Validators.compose([
      Validators.required])),
      firstName: new FormControl('', Validators.compose([
        Validators.required])),
        lastName: new FormControl('', Validators.compose([
        Validators.required]))  
  });
  constructor(private router: Router, private userService : UserService) { }
  user : any;
  submitForm(formValue : any){
    for (const key in this.validateRegister.controls) {
      if (this.validateRegister.controls.hasOwnProperty(key)) {
        this.validateRegister.controls[key].markAsDirty();
        this.validateRegister.controls[key].updateValueAndValidity();
      }
    }
    this.userService.register(formValue.value.userName, formValue.value.password, formValue.value.firstName, formValue.value.lastName).subscribe(
      (formValue)=>{
      this.user = formValue;
      alert("Successfully registered")
      this.router.navigate(['/login']);
      },
      (error) => {
        if(error.status == 401){
            alert("Bad credentials 401")
        }else{
            alert(`Error with status ${error.status}`)
        }
      }
    )
  }
  cancel(formValue : any){
    formValue.reset();
  }

}
