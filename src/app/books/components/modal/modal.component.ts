import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  constructor() { }
  @Input() isVisible!: boolean;
  @Output() isVisibleEvent = new EventEmitter<boolean>();
  @Output() authorAdded = new EventEmitter<any>();

  closeModal(status: any){
    this.isVisible = status;
    this.isVisibleEvent.emit(this.isVisible);
  }

  notifyAddForm(author: any){
      this.authorAdded.emit(author)
  }
  ngOnInit(): void {
  }

}
