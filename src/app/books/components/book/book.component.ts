import { Component, Inject, Input, OnInit } from '@angular/core';
import { BookService } from '../../services/book.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {

  book : any;
  id: any;
  creationDate: any;

  constructor(private bookService : BookService, private route: ActivatedRoute) { 
    this.book = this.bookService.getBook;
    this.id = this.route.snapshot.paramMap.get('bookId');
  }
  
  ngOnInit(): void {
    this.bookService.getBook(parseInt(this.id)).subscribe( (data) => {
      this.book = data
      this.creationDate = new Date(data.creationDate).toDateString()
    });
  }

}
