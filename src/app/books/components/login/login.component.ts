import { Component, OnInit} from '@angular/core';
import {  FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IUserInfo } from '../../interfaces/IUserInfo';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent{

  constructor(private authService : AuthService, private router: Router) { }

  userName : any;
  password : any;
  validateLogin = new FormGroup({
    userName: new FormControl('', Validators.compose([
      Validators.required])),
    password: new FormControl('', Validators.compose([
      Validators.required]))
  });
  submitForm(formValue : any){
    console.log(formValue.value)
    for (const key in this.validateLogin.controls) {
      if (this.validateLogin.controls.hasOwnProperty(key)) {
        this.validateLogin.controls[key].markAsDirty();
        this.validateLogin.controls[key].updateValueAndValidity();
      }
    }
    this.authService.loginUser(formValue.value.userName, formValue.value.password).subscribe(
      (data) => {
          const userInfo = <IUserInfo> data;
          localStorage.setItem("token", userInfo.token);
          localStorage.setItem("username", userInfo.username);
          alert("Successfully logged in")
          this.router.navigate(['/books']);
      },
      (error) => {
          if(error.status == 401){
              alert("Bad credentials 401")
          }else{
              alert(`Error with status ${error.status}`)
          }
      }
  )
  }
  cancel(formValue : any){
    formValue.reset();
  }
}
