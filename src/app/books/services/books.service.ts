import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class BooksService {
  private booksUrl = environment.baseUrlV3 + '/books'
  constructor(private http: HttpClient) { }
  headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': `Bearer ${localStorage.getItem("token")}`});

  getBooks(): Observable<any>{
    return this.http.get<any>(this.booksUrl);
  }  
  addBook(book : any): Observable<any>{
    return this.http.post<any>(this.booksUrl, book, {headers : this.headers});
  }  
  getScrollBooks(page:number, pageSize:number): Observable<any>{
    const params = new HttpParams()
      .set('page', page)
      .set('pageSize', pageSize);
    return this.http.get<any>(this.booksUrl,{params})
  }
}
