import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { IUser } from "../interfaces/IUser";
import { IUserInfo } from "../interfaces/IUserInfo";
import { environment } from "src/environments/environment";
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
  })
export class AuthService{

    constructor(private http: HttpClient, private router: Router) { }

    private loginPath = environment.baseUrlV3 + '/auth/login'
    currentUser!: IUser;
    loginUser(userName: string, password: string){
        this.currentUser={
            username : userName,
            password : password
        };
        return this.http.post(this.loginPath, this.currentUser)
    }
    
}