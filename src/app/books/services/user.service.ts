import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userUrl = environment.baseUrlV3 + '/users/profile';
  currentUser : any;
  constructor(private http: HttpClient, private router: Router) { }
  private registerPath = environment.baseUrlV3 + '/auth/register'

  getUser(): Observable<any>{
    return this.http.get<any>(this.userUrl);
  }

  register(userName: string, password: string, firstName : string, lastName : string){
    this.currentUser={
        username : userName,
        password : password,
        firstName : firstName,
        lastName : lastName
    };
    var headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json'});
    return this.http.post(this.registerPath, JSON.stringify(this.currentUser), { headers: headers })
}
}
