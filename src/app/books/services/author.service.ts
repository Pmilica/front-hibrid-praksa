import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  private authorsUrl = environment.baseUrlV3 + "/authors"
  constructor(private http: HttpClient) { }
  headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': `Bearer ${localStorage.getItem("token")}`});

  getAllAuthors(): Observable<any>{
    return this.http.get<any>(this.authorsUrl, {headers : this.headers});
  }

  addAuthor(author: any): Observable<any>{
    return this.http.post<any>(this.authorsUrl, author, {headers: this.headers})
  }
}
