import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HttpClient} from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  
  private bookUrl = environment.baseUrlV3 + '/books/';
  constructor(private http: HttpClient) { }

  getBook(id : number): Observable<any>{
    return this.http.get<any>(this.bookUrl + id);
  }
}
