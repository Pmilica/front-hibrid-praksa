import { Component, OnInit } from '@angular/core';
import { BooksService } from './services/books.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss'],
})
export class BooksComponent implements OnInit {
   books: any = [];
   paging=0;
   private pageSize = environment.PAGE_SIZE;
  constructor(private booksService: BooksService, private router: Router) { }
  ngOnInit(): void {
    this.booksService.getScrollBooks(this.paging, this.pageSize).subscribe(books=>this.books=books);

  }
  detailsClicked(book: any): void {
    this.router.navigate([`/book/${book.id}`])
  }
  onScrollDown() {
    this.paging +=1;
    this.booksService.getScrollBooks(this.paging, this.pageSize).subscribe(books=>this.books.push(...books));
  }

}
