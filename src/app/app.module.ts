import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NzListModule } from 'ng-zorro-antd/list';
import { CommonModule } from '@angular/common';  
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzImageModule } from 'ng-zorro-antd/experimental/image';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzSelectModule } from 'ng-zorro-antd/select';

import { AppComponent } from './app.component';
import { BookComponent } from './books/components/book/book.component';
import { BooksComponent } from './books/books.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AuthorsComponent } from './books/components/authors/authors.component';
import { AuthorComponent } from './books/components/author/author.component';
import { HeaderComponent } from './books/components/header/header.component';
import { ButtonComponent } from './books/components/button/button.component';
import { InfoComponent } from './books/components/info/info.component';
import { NavbarComponent } from './books/components/navbar/navbar.component';
import { LoginComponent } from './books/components/login/login.component';
import { RegisterComponent } from './books/components/register/register.component';
import { AddEditBookComponent } from './books/components/add-edit-book/add-edit-book.component';
import { NzModalModule } from 'ng-zorro-antd/modal';

import {NZ_I18N} from 'ng-zorro-antd/i18n';
import { en_US} from 'ng-zorro-antd/i18n';
import { ModalComponent } from './books/components/modal/modal.component';
import { FormAddAuthorComponent } from './books/components/form-add-author/form-add-author.component';


@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    BooksComponent,
    AuthorsComponent,
    AuthorComponent,
    HeaderComponent,
    ButtonComponent,
    InfoComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    AddEditBookComponent,
    ModalComponent,
    FormAddAuthorComponent
    ],
  imports: [
    BrowserModule,
    FormsModule,
    NzSelectModule,
    AppRoutingModule,
    HttpClientModule,
    NzListModule,
    CommonModule,
    NzFormModule,
    NzModalModule,
    NzDatePickerModule,
    ReactiveFormsModule,
    NzCollapseModule,
    NzCardModule,
    NzGridModule,
    InfiniteScrollModule,
    NzImageModule,
    NzMenuModule,
    BrowserAnimationsModule
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
